package com.wsepr.flexreport.core
{
    import mx.core.UIComponent;

    /**
    * 表示MXML文档中的一个节点
    * */
    public class Element
    {
        /**
        * 父级节点
        * */
        public var parent:Element = null;
        
        /**
        * 节点数据，可能是UIComponent、String或其他类型
        * */
        public var data:Object;
        
        /**
        * 下级节点
        * */
        private var _children:Array = [];
        
        /**
        * 节点类型
        * */
        public var type:int;
        
        /**
        * 节点名称
        * */
        public var tagName:String;
        
        /**
        * 对应的mxml标签
        * */
        public var mxml:XML;
        
        /**
        * 增加一个下级节点
        * */
        public function addChild(elem:Element):void{
            _children.push(elem);
        }
        
        /**
        * 下级节点
        * */
        public function get children():Array{
            return this._children;
        }
        
        public function toUIComponent():UIComponent{
            return this.data as UIComponent;
        }
    }
}