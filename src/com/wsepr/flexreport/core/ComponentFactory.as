package com.wsepr.flexreport.core
{
    import flash.net.registerClassAlias;
    import flash.utils.ByteArray;
    
    import mx.core.IFactory;
    import mx.core.UIComponent;
    
    /**
    * 用于内嵌组件
    * */
    public class ComponentFactory implements IFactory
    {
        private var component:Object;
        
        public function ComponentFactory(com:Object)
        {
            this.component = com;
        }
        
        public function newInstance():*
        {
            if(component is MxmlParser){
                var parser:MxmlParser = component as MxmlParser;
                var page:FlexPage = parser.parseMXML();
                return page.ui;
            }else{
                return component;
            }
        }
        
    }
}