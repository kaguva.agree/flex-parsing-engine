package com.wsepr.flexreport.core
{
    import mx.core.UIComponent;

    public class FlexPage
    {
        private var _pageContext:Object = new Object();
        
        private var _ui:UIComponent;
        
        public function get pageContext():Object{
            return this._pageContext;
        }
        
        public function putComponentToContext(component:Object):void{
            if(component != null && component is UIComponent && component.id != null){
                _pageContext[component.id] = component;
            }
        }
        
        public function get ui():UIComponent{
            return this._ui;
        }
        
        public function set ui(component:UIComponent):void{
            this._ui = component;
        }
        
        /**
        * 清空上下文环境
        * */
        public function clearContext():void{
            _pageContext = new Object();
        }
    }
}