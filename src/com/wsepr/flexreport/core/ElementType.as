package com.wsepr.flexreport.core
{
    /**
    * MXML 代码中的元素类型
    * */
    public final class ElementType
    {
        /**
        * 非容器控件类，如TextInput、DataGrid
        * */
        public static const COMPONENT:int = 0;
        
        /**
         * 容器类，如HBox、Group、Canvas
         * */
        public static const CONTAINER:int = 1;
        
        /**
         * 属性类，该类型标签只是父标签的一个属性
         * */
        public static const PROPERTY:int = 2;
        
        /**
         * 一般的对象类型，如果DataGridColumn
         * */
        public static const OBJECT:int = 3;
        
        /**
        * 文本类型
        * */
        public static const TEXT:int = 4;
        
        /**
        * 命名控件fx=”http://ns.adobe.com/mxml/2009″下的标签
        * */
        public static const FX_TAG:int = 5;
    }
}