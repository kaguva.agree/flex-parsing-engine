该项目实现了动态解析MXML并实时生成界面，项目中使用了一个第三方类库D.eval实现了简单的actionscript支持。

关于D.eval的介绍可以看http://static.oschina.net/p/deval

项目使用Flex4.1开发，这个项目只是一个试验品，半成品都说不上。现在这个项目不会再维护，有兴趣的人可以继续完善。

## 效果图

![效果图](http://static.oschina.net/uploads/space/2014/0626/174656_dabq_92866.png)